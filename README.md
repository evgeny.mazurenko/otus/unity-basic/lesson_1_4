# ДЗ занятия "Введение в C#. Часть 1"

1. Установлена среда разработки JetBrains Rider.
2. Переменные различных числовых типов объявлены в методе _Main.Start_.
3. Преобразования данных числовых типов в один из 4-х целевых (byte, int, float, decimal) реализованы в методах _Operation.NumberTo*_ (логика преобразования) и методе _Main.ConvertNumericArrTo4Types_ (преобразование массива переменных из п.1 в каждый из целевых типов).
4. Генерация строки, содержащей исходные числовые данные и преобразованные их byte-значения, а также её печать осуществляются в методе _Main.GetListedNumericArrWithByteConversion_.
5. Запись сгенерованной строки методов из п.4 осуществляется в файл **numeric_conversions.txt** (файл добавлен в репозиторий) в режиме перезаписи в методе _Main.WriteConvertedNumericArrToFile_.