using System.Collections;
using System.Collections.Generic;
using System.IO;
using OtusHomeWork;
using UnityEngine;
using Object = System.Object;

public class Main : MonoBehaviour
{
    private const string FILE_PATH = "./numeric_conversions.txt";
    
    // Start is called before the first frame update
    void Start()
    {
        byte nByte = 1;
        short nShort = 2;
        int nInt = 3;
        long nLong = 4L;
        float nFloat = 5.1f;
        double nDouble = 6.2;
        decimal nDecimal = 7.3m;

        object[] numericArr = {nByte,nShort,nInt,nLong,nFloat,nDouble,nDecimal};

        ConvertNumericArrTo4Types(numericArr);
        GetListedNumericArrWithByteConversion(numericArr);
        WriteConvertedNumericArrToFile(numericArr);
    }

    private void ConvertNumericArrTo4Types(object[] numericArr)
    {
        ConvertNumericArrToByte(numericArr);
        ConvertNumericArrToInt(numericArr);
        ConvertNumericArrToFloat(numericArr);
        ConvertNumericArrToDecimal(numericArr);
    }

    private string GetListedNumericArrWithByteConversion(object[] numericArr)
    {
        Debug.Log("Source numeric arr with byte converted values:");
        string result = "";
        foreach (object num in numericArr)
        {
            result += $"Source value: {num} [{num.GetType()}]; converted byte value: {Operation.NumberToByte(num)}\n";
        }
        Debug.Log(result);
        return result;
    }

    private void WriteConvertedNumericArrToFile(object[] numericArr)
    {
        Debug.Log("Print byte conversion to file " + FILE_PATH);
        using (StreamWriter sw = new StreamWriter(FILE_PATH, false))
        {
            sw.Write(GetListedNumericArrWithByteConversion(numericArr));
        }
    }

    private byte[] ConvertNumericArrToByte(object[] numericArr)
    {
        Debug.Log("Convert to Byte");
        byte[] result = new byte[numericArr.Length];
        for (int i=0; i < numericArr.Length; i++)
        {
            byte converted = Operation.NumberToByte(numericArr[i]);
            Debug.Log(converted);
            result[i] = converted;
        }
        return result;
    }
    
    private int[] ConvertNumericArrToInt(object[] numericArr)
    {
        Debug.Log("Convert to Int");
        int[] result = new int[numericArr.Length];
        for (int i=0; i < numericArr.Length; i++)
        {
            int converted = Operation.NumberToInt(numericArr[i]);
            Debug.Log(converted);
            result[i] = converted;
        }
        return result;
    }
    
    private float[] ConvertNumericArrToFloat(object[] numericArr)
    {
        Debug.Log("Convert to Float");
        float[] result = new float[numericArr.Length];
        for (int i=0; i < numericArr.Length; i++)
        {
            float converted = Operation.NumberToFloat(numericArr[i]);
            Debug.Log(converted);
            result[i] = converted;
        }
        return result;
    }
    
    private decimal[] ConvertNumericArrToDecimal(object[] numericArr)
    {
        Debug.Log("Convert to Decimal");
        decimal[] result = new decimal[numericArr.Length];
        for (int i=0; i < numericArr.Length; i++)
        {
            decimal converted = Operation.NumberToDecimal(numericArr[i]);
            Debug.Log(converted);
            result[i] = converted;
        }
        return result;
    }
    
}
