using System;

namespace OtusHomeWork
{
    public static class Operation
    {
        
        public static byte NumberToByte(object num)
        {
            if (ObjectIsNumber(num))
            {
                //Приведение типов по подобию (byte)num не работает для обернутых в object значений,
                //поэтому использую для конверсии интерфейс IConvertible. 
                return ((IConvertible)num).ToByte(null);
            }
            return 0;
        }
        
        public static int NumberToInt(object num)
        {
            if (ObjectIsNumber(num))
            {
                return ((IConvertible)num).ToInt32(null);
            }
            return 0;
        }
        
        public static decimal NumberToDecimal(object num)
        {
            if (ObjectIsNumber(num))
            {
                return ((IConvertible)num).ToDecimal(null);
            }
            return 0;
        }
        
        public static float NumberToFloat(object num)
        {
            if (ObjectIsNumber(num))
            {
                //Метод ToFloat в интерфейсе IConvertible отсутствует,
                //поэтому привожу сначала к Double, а затем понижаю точность через приведение к целевому типу. 
                return (float)((IConvertible)num).ToDouble(null);
            }
            return 0;
        }
        
        

        private static bool ObjectIsNumber(object obj)
        {
            return obj is byte 
                or short 
                or int 
                or long 
                or float 
                or double 
                or decimal;
        }
    }
}